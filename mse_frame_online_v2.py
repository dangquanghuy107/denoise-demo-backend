"""
Created in 2020
Author: Thanh Duong
Denoising based on mmse & 
"""
import os
import yaml
import numpy as np
from scipy.io import wavfile
import math
from time import time

def to_float(_input): # make input to be float64
    if _input.dtype == np.float64:
        return _input, _input.dtype
    elif _input.dtype == np.float32:
        return _input.astype(np.float64), _input.dtype
    elif _input.dtype == np.uint8:
        return (_input - 128) / 128., _input.dtype
    elif _input.dtype == np.int16:
        return _input / 32768., _input.dtype
    elif _input.dtype == np.int32:
        return _input / 2147483648., _input.dtype
    raise ValueError('Unsupported wave file format')

def from_float(_input, dtype): # change input into dtype data type
    if dtype == np.float64:
        return _input, np.float64
    elif dtype == np.float32:
        return _input.astype(np.float32)
    elif dtype == np.uint8:
        return ((_input * 128) + 128).astype(np.uint8)
    elif dtype == np.int16:
        return (_input * 32768).astype(np.int16)
    elif dtype == np.int32:
        print(_input)
        return (_input * 2147483648).astype(np.int32)
    raise ValueError('Unsupported wave file format')

 
def mmse(x, fs=16000, initial_noise_frames=6, window_size=0.02, vad_threshold=0.15, alpha=0.98, alpha_d=0.98, minPrioriSNR=0.0001, maxPosteriorSNR=40, saved_params=None):
 
     wlen = int(math.floor(window_size * fs))  
     if wlen % 2 == 1:
         wlen = wlen + 1
        
     len1 = int(math.floor(0.5*wlen)) 
     len2 = int(wlen - len1)
 
     win = np.hanning(wlen)
     win = win * len2 / np.sum(win)
     nFFT = 1 * wlen 
 
     x_old = np.zeros(len1)   
     Xk_prev = np.zeros(len1) 
     xout = [] 
 
     if saved_params is None:
         noise_mean = np.zeros(nFFT)
         for j in range(0, wlen*initial_noise_frames, wlen):
             noise_mean = noise_mean + np.absolute(np.fft.fft(win * x[j:j + wlen], nFFT, axis=0))
         noise_var = (noise_mean / initial_noise_frames) ** 2 
     else:
         noise_var = saved_params['noise_var']
         Xk_prev = saved_params['Xk_prev']
         x_old = saved_params['x_old']
      
     k = 0
     segment = win * x[0 : wlen] 
     while segment is not None:
         #Step 1
 
         spec = np.fft.fft(segment, nFFT, axis=0)
         spec_amplitude = np.absolute(spec)
         spec_power = spec_amplitude ** 2
 
         # Step 2
         gammak = np.minimum(spec_power / noise_var, maxPosteriorSNR) 
          
         # Step 3 
         if Xk_prev.all() == 0:  
             ksi = alpha + (1 - alpha) * np.maximum(gammak - 1, 0)
         else:
             ksi = alpha * Xk_prev / noise_var + (1 - alpha) * np.maximum(gammak - 1, 0) 
             ksi = np.maximum(minPrioriSNR, ksi)
         
         # Step 4
         G_MSE = ksi / (1 + ksi) 
         vk = G_MSE * gammak     
         
         log_sigma_k = vk - np.log(1 + ksi)      
         vad_decision = np.sum(log_sigma_k)/wlen 
         if vad_decision < vad_threshold: 
             noise_var = alpha_d * noise_var + (1 - alpha_d) * spec_power 
# 
#         # Step 5      
#         G_LSA = G_MSE
         
         # Step 6
         spec_amplitude = spec_amplitude * G_MSE
         Xk_prev = spec_amplitude ** 2
         segment_out = np.fft.ifft(G_MSE * spec, nFFT, axis=0)
         segment_out = np.real(segment_out)
 
         xout = np.append(xout, x_old + segment_out[0:len1]) 
         x_old = segment_out[len1:wlen]
         
         k = k+len2
         if k+wlen < len(x):
             segment = win * x[k:k + wlen]  
         else:
             segment = None
 
     return xout, {'noise_var': noise_var, 'Xk_prev': Xk_prev, 'x_old': x_old}
 

def denoising_from_file(input_file, initial_noise_frames, window_size, vad_threshold, alpha, alpha_d, minPrioriSNR, maxPosteriorSNR, output_file=None):
    # Reading and segmenting an input wave file. 
    fs, data = wavfile.read(input_file, 'r')
    data, dtype = to_float(data)
    num_frames = len(data)
    chunk_size = int(np.floor(60*fs)) 
             
    output = np.array([], dtype=dtype)
    saved_params = None
    frames_read = 0
    time0 = time()
    
    while frames_read < num_frames:
        frames = num_frames - frames_read if frames_read + chunk_size > num_frames else chunk_size
        signal = data[frames_read:frames_read + frames] 
        frames_read = frames_read + frames
        
        _output, saved_params = mmse(signal, fs, initial_noise_frames, window_size, vad_threshold,
                                        alpha, alpha_d, minPrioriSNR, maxPosteriorSNR, saved_params)
        output = np.concatenate((output, from_float(_output, dtype)))
    
    print("\nProcessing Time (in seond) =",(time()-time0))
     
    if output_file is not None:
        wavfile.write(output_file, fs, output)
    return output

if __name__ == '__main__':
        
 #parameter settings for algorithm
    with open('parameter_settings.yaml', "r") as yaml_file:
        confs = yaml.load(yaml_file)
    
    sampling_rate = confs['LOGMMSE']['target_sampling_rate']
    initial_noise_frames = confs['LOGMMSE']['initial_noise_frames']
    window_size = confs['LOGMMSE']['stft_window_size']
    vad_threshold = confs['LOGMMSE']['vad_threshold']
    alpha = confs['LOGMMSE']['smooth_prioriSNR_estimation']
    alpha_d = confs['LOGMMSE']['smooth_noise_variance']
    minPrioriSNR = confs['LOGMMSE']['minPrioriSNR']
    maxPosteriorSNR = confs['LOGMMSE']['maxPosteriorSNR']

# denoising from input file
    path=''
    filename='Street_Duy_Female_01'
    fin = path+filename+'.wav'
    fout=path+filename+'_enhanced.wav'
    denoising_from_file(fin, initial_noise_frames, window_size, vad_threshold, alpha, alpha_d, minPrioriSNR, maxPosteriorSNR, fout)
    
# =============================================================================
#     pathin='../Data/Record/Duy_outputs/'
#     pathout='../Data/Record/Duy_outputs2/'
#     for r, d, f in os.walk(pathin):
#          for filename in f:
#                  fin = pathin+filename
#                  fout=pathout+filename[:len(filename)-4]+'_MSE.wav'
#                  mmse_from_file(fin, initial_noise_frames, window_size, vad_threshold, alpha, alpha_d, minPrioriSNR, maxPosteriorSNR, fout)
#  
#    
# =============================================================================
