# Denoise demo

## Project setup

- backend 

```
docker-compose build
docker-compose up
```

```
docker build --build-arg VUE_APP_API_URL={your api url goes here} . -t denoise-demo:production
docker run -p 80:80 -d denoise-demo:production
```

Trong đó, API_URL là `http:// + IP + :5000/api`