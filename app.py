import asyncio
from sanic import Sanic, response
# from sanic.response import html
import socketio
# from logmmse_frame_online_v1 import logmmse, logmmse_from_file
from mse_frame_online_final import mmse
import numpy as np
from crn_code_final.demo import *

from scipy.io import wavfile


sio = socketio.AsyncServer(async_mode='sanic', cors_allowed_origins='*')
app = Sanic()
sio.attach(app) 


# DNN Model
TRAINED_MODEL_PATH = 'crn_code_final/checkpoints/final.pt'
infer_streamer = StreamingInference(TRAINED_MODEL_PATH)

input1 = np.array([], dtype='float32')
output1 = np.array([], dtype='float32')
output2 = np.array([], dtype='float32')


@sio.event
async def connected(sid, message):
    await sio.emit('my_response', {'data': message['data']}, room=sid)

@sio.event
async def noise_1(sid, message):
    global output1, output2
    global input1
    temp = np.array(message, dtype='float32')
    input1 = np.concatenate((input1, temp))
    _output1, out = mmse(temp)
    output1 = np.concatenate((output1, _output1.astype(np.float32)))
    await sio.emit('noise_responses_1', _output1.tolist())


@sio.event
async def noise_2(sid, message):
    global output2
    temp = np.array(message, dtype='float32')
    _output2 = infer_streamer.inference(temp)
    output2 = np.concatenate((output2, _output2.astype(np.float32)))
    await sio.emit('noise_responses_2', _output2.tolist())


@sio.event
async def get_result(sid, message):
    global output1, output2
    global tot_len, input1
    if message == 'Output':
        wavfile.write('/output/input_file.wav', 16000, input1)
        wavfile.write('/output/output_file_mmse.wav', 16000, output1)
        wavfile.write('/output/output_file_dnn.wav', 16000, output2)
        input1 = np.array([], dtype='float32')
        output1 = np.array([], dtype='float32')
        output2 = np.array([], dtype='float32')
        await sio.emit('result', output1.tolist())
    else:
        await sio.emit('result', {'data': 'Invalid'})


@sio.event
async def disconnect_request(sid):
    await sio.disconnect(sid)


@sio.event
async def connect(sid, environ):
    await sio.emit('my_response', {'data': 'Connected', 'count': 0}, room=sid)


@sio.event
def disconnect(sid):
    print('Client disconnected')


@app.route('/api/download_input')
def download_input(request):
    return response.file('/output/input_file.wav', 
                        filename='input_file.wav',
                        headers={'Access-Control-Allow-Origin': '*',
                                'Accept-Ranges': 'bytes'})


@app.route('/api/download_output_mmse')
def download_output_mmse(request):
    return response.file('/output/output_file_mmse.wav', 
                        filename='output_file_mmse.wav',
                        headers={'Access-Control-Allow-Origin': '*',
                                'Accept-Ranges': 'bytes'})


@app.route('/api/download_output_dnn')
def download_output_dnn(request):
    return response.file('/output/output_file_dnn.wav', 
                        filename='output_file_dnn.wav',
                        headers={'Access-Control-Allow-Origin': '*',
                                'Accept-Ranges': 'bytes'})


@app.route('/api/demo')
def get_demo(request):
    result = []
    path = os.path.join('demo_files', 'input')
    for _root, _dirs, files in os.walk(path):
        for f in files:
            result.append(f.split('.')[0])

    result.sort(reverse=True)
    return response.json(result, headers={'Access-Control-Allow-Origin': '*',
                                        'Accept-Ranges': 'bytes'})
            


@app.route('/api/demo/input/<filename>')
def download_demo_input(request, filename):
    input_path = os.path.join('demo_files', 'input')
    for root, _dirs, files in os.walk(input_path):
        for f in files:
            if filename in f: 
                return response.file(os.path.join(root, filename + '.wav'),
                        filename=filename + '.wav', 
                        headers={'Access-Control-Allow-Origin': '*',
                                'Accept-Ranges': 'bytes'})


@app.route('/api/demo/output/<filename>')
def download_demo_output(request, filename):
    output_path = os.path.join('demo_files', 'output')
    for root, _dirs, files in os.walk(output_path):
        for f in files:
            if filename in f: 
                return response.file(os.path.join(root, filename + '.wav'),
                        filename=filename + '.wav', 
                        headers={'Access-Control-Allow-Origin': '*',
                                'Accept-Ranges': 'bytes'})


app.static('/static', './statodeic')
