### Giới thiệu
Code thực hiện các công việc sau:
- Tạo ra một bộ dataset cho huấn luyện mô hình CRN. Bộ dataset khi
được tạo ra bao gồm hai tập train và valid, mỗi tập bao gồm các file wav
được lưu dưới dạng .npy của noisy speech (là clean speech sau khi được
mix với nhiễu) và clean speech tương ứng.
- Huấn luyện model với data đã tạo ra:
    + Hai model đã được implement là CRNN (17 triệu params) giống
trong paper và ShallowerCRNN_BiggerKernel là model thu nhỏ (8.4
triệu params). Xem trong `model.py`.
    + Gradient accumulation và GroupNorm (trong paper dùng
BatchNorm) được sử dụng trong training để có thể train model với batch
size đủ lớn (batch_size 16) trên GPU 11G RAM. Xem trong `train.py`.

    ![Screenshot](images/intro_1.PNG)
    
    ![Screenshot](images/intro_2.PNG)
- Sau khi training hoàn tất (khoảng 70 epoch), thực hiện inference để thử
chất lượng model, tạo ra cleaned speech từ các file test là các noisy
speech
### Run the code
Sau đây là hướng dẫn chạy code để tạo ra bộ dữ liệu, huấn luyện model và thử nghiệm mô hình.
- Bước 1. Download data gồm: `speech dataset`, `metadata`, `noise data` tại link
này:
https://drive.google.com/file/d/1rR9F9sMzDVWaCxqWoPmmYGvwqlZgAYPm/view?usp=sharing. Trong đó:
    + `speech dataset`: gồm các speaker, trong mỗi thư mục speaker có
các file speech của speaker đó.
    ![Screenshot](images/step1.png)
    + meta data: gồm 4 file `male_speakers.npy`, `female_speakers.npy`,
`val_male_speakers.npy` và `val_female_speakers.npy` là các list speaker ID
đã được chọn ra cho train và valid. Ví dụ `speaker_013` là một male
speaker được chọn cho training. Các train speaker được chọn theo tiêu chí
là có trên 90 files. Synthesized dataset được tạo ra với số speaker cân
bằng về giới tính.
    + noise data: gồm hai tập noise được thu từ môi trường thực tế gần
với ứng dụng hướng tới, xem trong `noise_huynv22` và `noise_internet`.

- Bước 2. Vào file `config.py` và sửa các đường dẫn như sau:
    + `META_DATA_DIR` là thư mục chứa các file .npy là list speaker
ID, `SPEECH_PATH` là thư mục speech dataset nói trên, `NOISE_BY_HUYNV22` và `NOISE_FROM_INTERNET` là hai thư mục
chứa noise.

    ![Screenshot](images/step2.png)
    + `SYNTHESIZED_DATA_PATH` là thư mục dùng để lưu
synthesized dataset sau khi được tạo ra, `CHECKPOINT_PATH` là để lưu
model trong quá trình training. `OUTPUT_PATH` là thư mục lưu các file
output audio khi inference.

- Bước 3. Thực hiện synthesize data để tạo ra bộ dữ liệu cho huấn luyện
model. Chạy file `synthesize_data.py`, mỗi file speech sẽ được mix với hai loại nhiễu, mỗi loại nhiễu sẽ được mix thành 6 mức SNR khác nhau. Sau khi chạy xong sẽ được thư mục `SYNTHESIZED_DATA_PATH` như sau:

    ![Screenshot](images/step3.png)

- Bước 4. Train model, chạy file train.py​. Trong quá trình training,
checkpoint với best loss trên tập valid sẽ được lưu ra thư mục
`CHECKPOINT_PATH` và các logs về train/valid loss cũng được ghi ra
.txt file để tiện theo dõi.
    
    ![Screenshot](images/step4.png)
    
- Bước 5. Vào file `convert_checkpoint.py` và sửa `BEST_MODEL_FNAME` thành tên file của best checkpoint được lưu trong `CHECKPOINT_PATH` (như ở đây là `model_72_0.070202.pt`), sau đó chạy file này. Mục đích của code này là để lấy riêng phần weight của mô hình đã huấn luyện xong ra và save thành file `.pt`, vì khi sử dụng, source code của model có thay đổi đó là truyền thêm hidden state `h0`, `c0` vào `forward()`.

- Bước 6. Để thử nghiệm mô hình đã huấn luyện, vào file `demo.py`, sửa `test_audio_dir` trong hàm `main()` thành đường dẫn thư mục chứa các file noisy audio định dạng `.wav`. 
    Khi sử dụng mô hình cần đến những class sau:
    + `STFT`: thực hiện các bước preprocess tín hiệu từ miền thời gian sang tần số và ngược lại.
    + `CRNN`: kiến trúc của mô hình
    + `StreamingInference`: với mỗi lần inference một file audio sẽ phải khởi tạo lại object của class này. 
        + Khi khởi tạo, model đã train sẽ được load vào từ `ckpt_path`; `h_last` (long-term dependency) và `c_last` (short-term dependency) được khởi tạo là zero.
        + Method inference của class nhận vào biến `segment` là đoạn noisy speech cần enhance (duration ngắn, từ 0.5 - 1s) và trả về `enhanced_seg` là đoạn speech sau khi đã được giảm nhiễu. Mỗi lần `inference()`, `self.h_last` và `self.c_last` sẽ được cập nhật (là output của model cùng với enhanced magnitude STFT của segment).
        + Xem phần main để rõ hơn cách sử dụng, phần chương trình trong main thực hiện đọc các file audio (có nhiễu) từ thư mục `test_audio_dir`; với mỗi file, cắt thành các segment dài `SEG_DURATION` giây (1s) liên tiếp trên miền thời gian và inference từng segment này. Các output segment từ cùng một file ban đầu sẽ được ghép lại để thành một output audio rồi lưu ra thư mục `OUTPUT_PATH`. 
      
    Để inference ngay mà không cần qua các bước trên thì đổi đường dẫn `TRAINED_MODEL_PATH` trong file này thành `./checkpoints/final.pt` (dòng 18).
### Requirements
- python 3.6.8
- torch 1.2.0
- numpy 
- pandas 
- tqdm 
- librosa 0.7.1
- soundfile 0.9.0
 