import os 

SAMPLING_RATE = 16000
SEG_DURATION = 1.0 #s

WORKSPACE = '/mnt/4T/a/speech_enhancement'
# raw data 
## meta-data 
META_DATA_DIR = '/mnt/4T/a/speech_enhancement/synthesized_dataset/'
## speech data 
SPEECH_PATH = '/mnt/4T/a/speech_enhancement/wav_all'
## noise data 
NOISE_BY_HUYNV22 = '/mnt/4T/musan_audio/vietnamese/Vie_noise_wave_splited_60s'
NOISE_FROM_INTERNET = '/mnt/4T/musan_audio/noise/from-internet1'

# path to save synthesized dataset 
SYNTHESIZED_DATA_PATH = '/data0/khanhdtq/speech_enhancement/crn_realtime/data/preprocessed_dataset'
# os.makedirs(SYNTHESIZED_DATA_PATH, exist_ok=True)
# path to save model checkpoints during training 
CHECKPOINT_PATH = '/data0/khanhdtq/speech_enhancement/crn_voice_enhance_ckpts'
# os.makedirs(CHECKPOINT_PATH, exist_ok=True)
# path to save output wav when inferencing 
OUTPUT_PATH = os.path.join(WORKSPACE, 'output_wav_combine')
# os.makedirs(OUTPUT_PATH, exist_ok=True)
TMP_PATH = os.path.join(OUTPUT_PATH, 'temp')
# os.makedirs(TMP_PATH, exist_ok=True)
