import os
import librosa
import numpy as np
import pandas as pd
from tqdm import tqdm
import time

import torch
from torch.utils.data import Dataset, DataLoader 
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from torch.nn.utils.rnn import pad_sequence

from speech_dataloader import WavDataset, STFT, pad_to_longest, z_score
from model import CRNN, ShallowerCRNN_BiggerKernel
from loss import mse_loss 
from config import SAMPLING_RATE, SYNTHESIZED_DATA_PATH, CHECKPOINT_PATH

# os.environ["CUDA_VISIBLE_DEVICES"] = "2,3"

# = config =========================================
## path
LOG_PATH = os.path.join(CHECKPOINT_PATH, 'logs')
os.makedirs(LOG_PATH, exist_ok=True)

RESUME_CKPT = os.path.join(CHECKPOINT_PATH, 'model_16_0.035211.pt')

# training
RESUME_EPOCH = 0
N_EPOCHS = 100

TRAIN_BATCH_SIZE = 4
TEST_BATCH_SIZE = 2
ACCUMULATION_STEPS = 16 // TRAIN_BATCH_SIZE
Z_SCORE = False

LEARNING_RATE = 0.0002
BETA1 = 0.9

# = trainer ==========================================
def train(df_train, df_val, save_name='crn_3snr'):

    # log file
    log_fpath = os.path.join(LOG_PATH, save_name + '.txt')

    # dataloader
    ## train
    train_dataset = WavDataset(df=df_train)
    train_dataloader = DataLoader(dataset=train_dataset,
                                  batch_size=TRAIN_BATCH_SIZE,
                                  collate_fn=pad_to_longest,
                                  shuffle=True)
    ## valid
    valid_dataset = WavDataset(df=df_val)
    valid_dataloader = DataLoader(dataset=valid_dataset,
                                batch_size=TEST_BATCH_SIZE,
                                collate_fn=pad_to_longest,
                                shuffle=False)

    # net
    ## multi-GPUS training
    model = CRNN().cuda()
    # model = torch.nn.DataParallel(model, device_ids=[0, 1])
    if RESUME_EPOCH > 0:
        model = torch.load(RESUME_CKPT)
        print('checkpoint loaded!')

    # loss
    loss_function = mse_loss()

    # optimizer
    optimizer = torch.optim.Adam(
        params=model.parameters(),
        lr=LEARNING_RATE,
        betas=(BETA1, 0.999)
    )

    stft = STFT(filter_length=320, hop_length=160).cuda()
    best_loss = 10000.
    best_epoch = -1

    iter = 0
    for e in range(RESUME_EPOCH, N_EPOCHS):
        model.train()
        start_time = time.time()
        running_loss = 0.

        # train one epoch
        for preprocessed, clean, n_frames_list, _ in tqdm(train_dataloader):
            optimizer.zero_grad()

            preprocessed = preprocessed.cuda()
            clean = clean.cuda()

            # Mixture mag and Clean mag
            preprocessed_D = stft.transform(preprocessed)
            preprocessed_real = preprocessed_D[:, :, :, 0]
            preprocessed_imag = preprocessed_D[:, :, :, 1]
            preprocessed_mag = torch.sqrt(preprocessed_real ** 2 + preprocessed_imag ** 2)  # [batch, T, F]

            clean_D = stft.transform(clean)
            clean_real = clean_D[:, :, :, 0]
            clean_imag = clean_D[:, :, :, 1]
            clean_mag = torch.sqrt(clean_real ** 2 + clean_imag ** 2)

            if Z_SCORE:
                preprocessed_mag, _, _ = z_score(preprocessed_mag)
                clean_mag, _, _ = z_score(clean_mag)

            enhanced_mag = model(preprocessed_mag)
            loss = loss_function(enhanced_mag, clean_mag, n_frames_list)

            loss = loss / ACCUMULATION_STEPS
            loss.backward()
            if (iter + 1) % ACCUMULATION_STEPS == 0:
                optimizer.step()
                optimizer.zero_grad()

            iter += 1

            running_loss += loss.item() / len(train_dataloader)

        running_loss = running_loss * ACCUMULATION_STEPS
        # validate
        model.eval()
        val_running_loss = 0.
        for val_preprocessed, val_clean, val_n_frames_list, val_names in tqdm(valid_dataloader):
            val_preprocessed = val_preprocessed.cuda()
            val_clean = val_clean.cuda()

            # Mixture mag and Clean mag
            val_preprocessed_D = stft.transform(val_preprocessed)
            val_preprocessed_real = val_preprocessed_D[:, :, :, 0]
            val_preprocessed_imag = val_preprocessed_D[:, :, :, 1]
            val_preprocessed_mag = torch.sqrt(val_preprocessed_real ** 2 + val_preprocessed_imag ** 2)

            val_clean_D = stft.transform(val_clean)
            val_clean_real = val_clean_D[:, :, :, 0]
            val_clean_imag = val_clean_D[:, :, :, 1]
            val_clean_mag = torch.sqrt(val_clean_real ** 2 + val_clean_imag ** 2)

            if Z_SCORE:
                val_preprocessed_mag, val_preprocessed_mean, val_preprocessed_std = z_score(val_preprocessed_mag)
                val_clean_mag, _, _ = z_score(val_clean_mag)

            val_enhanced_mag = model(val_preprocessed_mag)
            val_loss = loss_function(val_enhanced_mag, val_clean_mag, val_n_frames_list)
            val_running_loss += val_loss.item() / len(valid_dataloader)

        elapsed = time.time() - start_time
        mesg = f'Epoch: {e + 1} - train_loss: {running_loss:8f} - valid_loss: {val_running_loss:8f} - time: {elapsed:.0f}s'
        print(mesg)
        with open(log_fpath, 'a') as log_writer:
            log_writer.write(mesg + '\n')

        if val_running_loss < best_loss:
            torch.save(model, os.path.join(CHECKPOINT_PATH, f'model_{e + 1}_{val_running_loss:8f}.pt'))
            best_loss = val_running_loss
            best_epoch = e + 1

    return {'best_loss': best_loss, 'best_epoch': best_epoch}

if __name__ == '__main__':
    # df train
    train_preprocessed = os.listdir(os.path.join(SYNTHESIZED_DATA_PATH, 'mixture'))
    train_clean = ['{}.npy'.format('_'.join(fname.split('_')[:-2])) for fname in train_preprocessed]
    train_preprocessed = [os.path.join(SYNTHESIZED_DATA_PATH, 'mixture', fname) for fname in train_preprocessed]
    train_clean = [os.path.join(SYNTHESIZED_DATA_PATH, 'clean', fname) for fname in train_clean]

    df_train = pd.DataFrame(list(zip(train_preprocessed, train_clean)), columns=['mixture', 'clean'])

    # df valid
    val_preprocessed = os.listdir(os.path.join(SYNTHESIZED_DATA_PATH, 'valid_mixture'))
    val_clean = ['{}.npy'.format('_'.join(fname.split('_')[:-1])) for fname in val_preprocessed]
    val_preprocessed = [os.path.join(SYNTHESIZED_DATA_PATH, 'valid_mixture', fname) for fname in val_preprocessed]
    val_clean = [os.path.join(SYNTHESIZED_DATA_PATH, 'valid_clean', fname) for fname in val_clean]
    df_val = pd.DataFrame(list(zip(val_preprocessed, val_clean)),
                                     columns=['mixture', 'clean'])

    train(df_train, df_val)