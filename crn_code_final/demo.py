import librosa
import numpy as np
import os

import soundfile as sf
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from torch.nn.utils.rnn import pad_sequence
import time 

from .config import CHECKPOINT_PATH, OUTPUT_PATH, SAMPLING_RATE, SEG_DURATION
# from config import SAMPLING_RATE, SEG_DURATION

# TRAINED_MODEL_PATH = os.path.join(CHECKPOINT_PATH, 'final.pt')
TRAINED_MODEL_PATH = './checkpoints/final.pt'

SEG_LENGTH = int(SEG_DURATION * SAMPLING_RATE)

def read_wave(fpath):
    y, _ = librosa.load(fpath, sr=SAMPLING_RATE)

    return y

class STFT(torch.nn.Module):
    def __init__(self, filter_length=1024, hop_length=512):
        super(STFT, self).__init__()

        self.filter_length = filter_length
        self.hop_length = hop_length
        self.forward_transform = None

        fourier_basis = np.fft.fft(np.eye(self.filter_length))
        window = np.sqrt(np.hamming(filter_length)).astype(np.float32)
        fourier_basis = fourier_basis * window
        cutoff = int((self.filter_length / 2 + 1))
        fourier_basis = np.vstack([np.real(fourier_basis[:cutoff, :]),
                                   np.imag(fourier_basis[:cutoff, :])])
        forward_basis = torch.FloatTensor(fourier_basis[:, None, :])
        forward_basis = torch.FloatTensor(fourier_basis[:, None, :])

        inv_fourier_basis = np.fft.fft(np.eye(self.filter_length))
        inv_fourier_basis = inv_fourier_basis / window
        cutoff = int((self.filter_length / 2 + 1))
        inv_fourier_basis = np.vstack([np.real(inv_fourier_basis[:cutoff, :]),
                                       np.imag(inv_fourier_basis[:cutoff, :])])
        inverse_basis = torch.FloatTensor(np.linalg.pinv(inv_fourier_basis).T[:, None, :])

        self.register_buffer('forward_basis', forward_basis.float())
        self.register_buffer('inverse_basis', inverse_basis.float())

    def transform(self, input_data):
        num_batches = input_data.size(0)
        num_samples = input_data.size(1)

        input_data = input_data.view(num_batches, 1, num_samples)
        forward_transform = F.conv1d(input_data,
                                     Variable(self.forward_basis, requires_grad=False),
                                     stride=self.hop_length,
                                     padding=0)
        cutoff = int((self.filter_length / 2) + 1)
        real_part = forward_transform[:, :cutoff, :]
        imag_part = forward_transform[:, cutoff:, :]

        res = torch.stack([real_part, imag_part], dim=-1)
        res = res.permute([0, 2, 1, 3])
        return res

    # (B,T,F,2)
    def inverse(self, stft_res):
        real_part = stft_res[:, :, :, 0].permute(0, 2, 1)
        imag_part = stft_res[:, :, :, 1].permute(0, 2, 1)
        recombine_magnitude_phase = torch.cat([real_part, imag_part], dim=1)

        inverse_transform = F.conv_transpose1d(recombine_magnitude_phase,
                                               Variable(self.inverse_basis, requires_grad=False),
                                               stride=self.hop_length,
                                               padding=0)
        return inverse_transform[:, 0, :]

    def forward(self, input_data):
        stft_res = self.transform(input_data)
        reconstruction = self.inverse(stft_res)
        return reconstruction

class CRNN(nn.Module):
    """
    Input: [batch size, channels=1, T, n_fft]
    Output: [batch size, T, n_fft]
    """
    def __init__(self):
        super(CRNN, self).__init__()
        # Encoder
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=(2, 3), stride=(1, 2))
        self.bn1 = nn.GroupNorm(1, 16)
        self.conv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=(2, 3), stride=(1, 2))
        self.bn2 = nn.GroupNorm(1, 32)
        self.conv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(2, 3), stride=(1, 2))
        self.bn3 = nn.GroupNorm(1, 64)
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=(2, 3), stride=(1, 2))
        self.bn4 = nn.GroupNorm(1, 128)
        self.conv5 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=(2, 3), stride=(1, 2))
        self.bn5 = nn.GroupNorm(1, 256)

        # LSTM
        self.LSTM1 = nn.LSTM(input_size=1024, hidden_size=1024, num_layers=2, batch_first=True)

        # Decoder
        self.convT1 = nn.ConvTranspose2d(in_channels=512, out_channels=128, kernel_size=(2, 3), stride=(1, 2))
        self.bnT1 = nn.GroupNorm(1, 128)
        self.convT2 = nn.ConvTranspose2d(in_channels=256, out_channels=64, kernel_size=(2, 3), stride=(1, 2))
        self.bnT2 = nn.GroupNorm(1, 64)
        self.convT3 = nn.ConvTranspose2d(in_channels=128, out_channels=32, kernel_size=(2, 3), stride=(1, 2))
        self.bnT3 = nn.GroupNorm(1, 32)

        self.convT4 = nn.ConvTranspose2d(in_channels=64, out_channels=16, kernel_size=(2, 3), stride=(1, 2),
                                         output_padding=(0, 1))
        self.bnT4 = nn.GroupNorm(1, 16)
        self.convT5 = nn.ConvTranspose2d(in_channels=32, out_channels=1, kernel_size=(2, 3), stride=(1, 2))
        self.bnT5 = nn.GroupNorm(1, 1)

        self.logit = nn.Softplus()

    def forward(self, x, h0, c0):
        # conv
        # (B, in_c, T, F)
        x.unsqueeze_(1)
        x1 = F.elu(self.bn1(self.conv1(x)))
        x2 = F.elu(self.bn2(self.conv2(x1)))
        x3 = F.elu(self.bn3(self.conv3(x2)))
        x4 = F.elu(self.bn4(self.conv4(x3)))
        x5 = F.elu(self.bn5(self.conv5(x4)))
        # reshape
        out5 = x5.permute(0, 2, 1, 3)
        out5 = out5.reshape(out5.size()[0], out5.size()[1], -1)
        # lstm
        lstm, (hn, cn) = self.LSTM1(out5, (h0, c0))
        # reshape
        output = lstm.reshape(lstm.size()[0], lstm.size()[1], 256, -1)
        output = output.permute(0, 2, 1, 3)
        # ConvTrans
        res = torch.cat((output, x5), 1)
        res1 = F.elu(self.bnT1(self.convT1(res)))
        res1 = torch.cat((res1, x4), 1)
        res2 = F.elu(self.bnT2(self.convT2(res1)))
        res2 = torch.cat((res2, x3), 1)
        res3 = F.elu(self.bnT3(self.convT3(res2)))
        res3 = torch.cat((res3, x2), 1)
        res4 = F.elu(self.bnT4(self.convT4(res3)))
        res4 = torch.cat((res4, x1), 1)
        # (B, o_c, T. F)
        res5 = self.bnT5(self.convT5(res4))
        out = self.logit(res5)

        return out.squeeze(), hn, cn 

class StreamingInference:

    def __init__(self, ckpt_path): 
        # model
        model = CRNN()
        checkpoint = torch.load(ckpt_path, map_location='cpu')
        model.load_state_dict(checkpoint['model_state_dict'])
        self.model = model 
        self.model.eval()
        # stft
        self.stft = STFT(filter_length=320, hop_length=160).to('cpu')
        # h_last, c_last 
        self.h_last = torch.zeros(2, 1, 1024)
        self.c_last = torch.zeros(2, 1, 1024)

    def inference(self, segment):

        wav_seg = [torch.tensor(segment).reshape(-1, 1)]
        wav_seg = pad_sequence(wav_seg).squeeze(2).permute(1, 0)

        seg_D = self.stft.transform(wav_seg)
        seg_real = seg_D[:, :, :, 0]
        seg_img = seg_D[:, :, :, 1]
        seg_mag = torch.sqrt(seg_real ** 2 + seg_img ** 2)

        if seg_mag.shape[1] >= 6:
            seg_mag = seg_mag.to('cpu')
            enhanced_mag_seg, hn, cn = self.model(seg_mag, self.h_last, self.c_last)
            enhanced_mag_seg = enhanced_mag_seg.detach().cpu()
            seg_mag = seg_mag.squeeze(1)
            enhanced_mag_seg = enhanced_mag_seg.unsqueeze(0)

            enhanced_real_seg = enhanced_mag_seg * seg_real[:, :enhanced_mag_seg.size(1), :] / seg_mag[:, :enhanced_mag_seg.size(1), :]
            enhanced_imag_seg = enhanced_mag_seg * seg_img[:, :enhanced_mag_seg.size(1), :] / seg_mag[:, :enhanced_mag_seg.size(1), :]

            enhanced_D_seg = torch.stack([enhanced_real_seg, enhanced_imag_seg], 3)
            enhanced_seg = self.stft.inverse(enhanced_D_seg)

            enhanced_seg = enhanced_seg.detach().cpu().squeeze().numpy()

            self.h_last, self.c_last = hn, cn 
            
            return enhanced_seg

        return segment 

if __name__ == '__main__':

    test_audio_dir = '/mnt/4T/a/speech_enhancement/ketqua_20200214/input'

    os.makedirs(os.path.join(OUTPUT_PATH, f'{SEG_DURATION}s_dnn_sp'), exist_ok=True) 
    for test_audio_fname in os.listdir(test_audio_dir):

        infer_streamer = StreamingInference(TRAINED_MODEL_PATH)

        test_audio_fpath = os.path.join(test_audio_dir, test_audio_fname)
        test_wav = read_wave(test_audio_fpath)

        out_segments = []
        for i in range(0, len(test_wav), SEG_LENGTH):
            end = min(len(test_wav), i + SEG_LENGTH)
            segment = test_wav[i: end]
            cleaned_segment = infer_streamer.inference(segment)

            out_segments.append(cleaned_segment)

        out_wav = np.hstack(out_segments)

        save_name = test_audio_fname.split('.')[0]
        sf.write(os.path.join(OUTPUT_PATH, f'{SEG_DURATION}s_dnn_sp', f'{save_name}.wav'), 
                                                        out_wav, SAMPLING_RATE)
            





    



