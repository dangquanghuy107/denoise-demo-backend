import os

import librosa
import numpy as np
import pandas as pd

import torch
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
import torch.nn.functional as F
from torch.autograd import Variable

from torch.nn.utils.rnn import pad_sequence

from tqdm import tqdm

SAMPLING_RATE = 16000

def z_score(m):
    mean = torch.mean(m, [1, 2])
    std_var = torch.std(m, [1, 2])

    # size: [batch] => pad => [batch, T, F]
    mean = mean.expand(m.size()[::-1]).permute(2, 1, 0)
    std_var = std_var.expand(m.size()[::-1]).permute(2, 1, 0)

    return (m - mean) / std_var, mean, std_var

# STFT =========================================================================================
class STFT(torch.nn.Module):
    def __init__(self, filter_length=1024, hop_length=512):
        super(STFT, self).__init__()

        self.filter_length = filter_length
        self.hop_length = hop_length
        self.forward_transform = None
        # scale = self.filter_length / self.hop_length
        # scale = 1
        fourier_basis = np.fft.fft(np.eye(self.filter_length))
        window = np.sqrt(np.hamming(filter_length)).astype(np.float32)
        fourier_basis = fourier_basis * window
        cutoff = int((self.filter_length / 2 + 1))
        fourier_basis = np.vstack([np.real(fourier_basis[:cutoff, :]),
                                   np.imag(fourier_basis[:cutoff, :])])
        forward_basis = torch.FloatTensor(fourier_basis[:, None, :])
        forward_basis = torch.FloatTensor(fourier_basis[:, None, :])

        inv_fourier_basis = np.fft.fft(np.eye(self.filter_length))
        inv_fourier_basis = inv_fourier_basis / window
        cutoff = int((self.filter_length / 2 + 1))
        inv_fourier_basis = np.vstack([np.real(inv_fourier_basis[:cutoff, :]),
                                       np.imag(inv_fourier_basis[:cutoff, :])])
        inverse_basis = torch.FloatTensor(np.linalg.pinv(inv_fourier_basis).T[:, None, :])

        self.register_buffer('forward_basis', forward_basis.float())
        self.register_buffer('inverse_basis', inverse_basis.float())

    def transform(self, input_data):
        num_batches = input_data.size(0)
        num_samples = input_data.size(1)

        input_data = input_data.view(num_batches, 1, num_samples)
        forward_transform = F.conv1d(input_data,
                                     Variable(self.forward_basis, requires_grad=False),
                                     stride=self.hop_length,
                                     padding=0)
        cutoff = int((self.filter_length / 2) + 1)
        real_part = forward_transform[:, :cutoff, :]
        imag_part = forward_transform[:, cutoff:, :]

        res = torch.stack([real_part, imag_part], dim=-1)
        res = res.permute([0, 2, 1, 3])
        return res

    # (B,T,F,2)
    def inverse(self, stft_res):
        real_part = stft_res[:, :, :, 0].permute(0, 2, 1)
        imag_part = stft_res[:, :, :, 1].permute(0, 2, 1)
        recombine_magnitude_phase = torch.cat([real_part, imag_part], dim=1)

        inverse_transform = F.conv_transpose1d(recombine_magnitude_phase,
                                               Variable(self.inverse_basis, requires_grad=False),
                                               stride=self.hop_length,
                                               padding=0)
        return inverse_transform[:, 0, :]

    def forward(self, input_data):
        stft_res = self.transform(input_data)
        reconstruction = self.inverse(stft_res)
        return reconstruction

# Dataset =====================================================================================
class WavDataset(Dataset):
    """
    Define train dataset
    """

    def __init__(self, df):

        mixture_wav_files = df['mixture'].tolist()
        clean_wav_files = df['clean'].tolist()

        print(f"\t Original length: {len(mixture_wav_files)}")

        self.length = len(mixture_wav_files)
        self.mixture_wav_files = mixture_wav_files
        self.clean_wav_files = clean_wav_files

    def __len__(self):
        return self.length

    def __getitem__(self, item):
        mixture_path = self.mixture_wav_files[item]
        clean_path = self.clean_wav_files[item]
        name = os.path.splitext(os.path.basename(clean_path))[0]

        mixture = np.load(mixture_path)
        clean = np.load(clean_path)
        assert mixture.shape == clean.shape

        n_frames = (len(mixture) - 320) // 160 + 1

        return mixture, clean, n_frames, name

# collate function ==========================================================================
def pad_to_longest(batch):
    mixture_list = []
    clean_list = []
    names = []
    n_frames_list = []

    for mixture, clean, n_frames, name in batch:
        mixture_list.append(torch.tensor(mixture).reshape(-1, 1))
        clean_list.append(torch.tensor(clean).reshape(-1, 1))
        n_frames_list.append(n_frames)
        names.append(name)

    mixture_list = pad_sequence(mixture_list).squeeze(2).permute(1, 0)
    clean_list = pad_sequence(clean_list).squeeze(2).permute(1, 0)

    return mixture_list, clean_list, n_frames_list, names
