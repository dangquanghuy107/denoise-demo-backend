import numpy as np
import librosa
import os

from config import SAMPLING_RATE
from config import META_DATA_DIR, SPEECH_PATH, NOISE_BY_HUYNV22, NOISE_FROM_INTERNET, SYNTHESIZED_DATA_PATH

HUYNV22_SNRs = [[-10, -3], [-2, 10], [11, 25]]
INTERNET_SNRs = [[-10, -3], [-2, 10], [11, 25]]

# = paths ========================================================================
internet_to_use = ['n1-noise-internet1.wav', 'n2-noise-internet1.wav', 'n3-noise-internet1.wav', 'n6-noise-internet1.wav',
                    'n7-noise-internet1.wav', 'n8-noise-internet1.wav', 'n9-noise-internet1.wav', 'n10-noise-internet1.wav',
                    'n18-noise-internet1.wav', 'n19-noise-internet1.wav', 'n20-noise-internet1.wav', 'n24-noise-internet1.wav',
                    'n70-noise-internet1.wav']

# path to save train data 
os.makedirs(os.path.join(SYNTHESIZED_DATA_PATH, 'clean'), exist_ok=True)
os.makedirs(os.path.join(SYNTHESIZED_DATA_PATH, 'mixture'), exist_ok=True)
# path to save valid data 
os.makedirs(os.path.join(SYNTHESIZED_DATA_PATH, 'valid_clean'), exist_ok=True)
os.makedirs(os.path.join(SYNTHESIZED_DATA_PATH, 'valid_mixture'), exist_ok=True)

# MIX NOISE =======================================================================
def read_wave(fpath):
    y, _ = librosa.load(fpath, sr=SAMPLING_RATE)

    return y

def cal_adjusted_rms(clean_rms, snr):
    a = float(snr) / 20
    noise_rms = clean_rms / (10 ** a)
    return noise_rms

def cal_amp(wf):
    buffer = wf.readframes(wf.getnframes())
    # The dtype depends on the value of pulse-code modulation. The int16 is set for 16-bit PCM.
    amptitude = (np.frombuffer(buffer, dtype="int16")).astype(np.float64)
    return amptitude

def cal_rms(amp):
    return np.sqrt(np.mean(np.square(amp), axis=-1))

def mix(clean_wav, noise_wav, snr):
    clean_amp = clean_wav
    noise_amp = noise_wav

    clean_rms = cal_rms(clean_amp)

    divided_noise_amp = np.resize(noise_amp, len(clean_amp))
    noise_rms = cal_rms(divided_noise_amp)

    adjusted_noise_rms = cal_adjusted_rms(clean_rms, snr)

    adjusted_noise_amp = divided_noise_amp * (adjusted_noise_rms / noise_rms)
    mixed_amp = (clean_amp + adjusted_noise_amp)

    # Avoid clipping noise
    max_int16 = np.iinfo(np.int16).max
    if mixed_amp.max(axis=0) > max_int16:
        reduction_rate = max_int16 / mixed_amp.max(axis=0)
        mixed_amp = mixed_amp * (reduction_rate)

    return mixed_amp

# CREATE TRAIN AND VALID DATASET ==================================================
def mix_noise_and_preprocess():

    # Create Train dataset 
    NUM_TRAIN_SPKRS = 30
    num_train_male = NUM_TRAIN_SPKRS // 2
    num_train_female = NUM_TRAIN_SPKRS - num_train_male
    train_male_spkrs = np.load(os.path.join(META_DATA_DIR, 'male_speakers.npy'))[: num_train_male]
    train_female_spkrs = np.load(os.path.join(META_DATA_DIR, 'female_speakers.npy'))[: num_train_female]
    train_spkrs = np.hstack((train_male_spkrs, train_female_spkrs))

    all_noise_files = os.listdir(NOISE_BY_HUYNV22)
    all_noise_files = [file_ for file_ in all_noise_files if ('thieu_nhi' not in file_) 
                        and ('thoi_su' not in file_) and ('nhac_san' not in file_)
                        and ('nhac_tre' not in file_)]
    noise_types = list(set(['_'.join(fname.split('_')[:-1]) for fname in all_noise_files]))
    noise_types = np.array(noise_types)
    noise_file_dict = {k: [] for k in noise_types}

    for noise_fname in all_noise_files:
        noise_name = '_'.join(noise_fname.split('_')[:-1])
        noise_file_dict[noise_name].append(noise_fname)

    for i, spkr in enumerate(train_spkrs):
        print(f'## speaker {i}th ...')
        try:
            for fname in os.listdir(os.path.join(SPEECH_PATH, spkr)):
                speech_name = fname.split('.')[0]
                # - speech ---------------------------------------------------------------------------------------------
                fpath = os.path.join(SPEECH_PATH, spkr, fname)
                speech = read_wave(fpath)
                ## save clean speech 
                np.save(os.path.join(SYNTHESIZED_DATA_PATH, 'clean', f'{speech_name}.npy'), speech)
                # - noise ----------------------------------------------------------------------------------------------
                ## noise huynv22
                this_type_list = noise_file_dict[noise_types[np.random.randint(len(noise_types))]]
                noise1_fpath = os.path.join(NOISE_BY_HUYNV22, this_type_list[np.random.randint(len(this_type_list))])
                noise1 = read_wave(noise1_fpath)
                for j, snr_range in enumerate(HUYNV22_SNRs):
                    snr_ = np.random.uniform(low=snr_range[0], high=snr_range[1])
                    mix_ = mix(speech, noise1, snr_)

                    np.save(os.path.join(SYNTHESIZED_DATA_PATH, 'mixture', f'{speech_fname}_huynv22_{j}.npy'), mix_)
                ## noise internet                 
                noise2_fpath = os.path.join(NOISE_FROM_INTERNET, internet_to_use[np.random.randint(len(internet_to_use))])
                noise2 = read_wave(noise2_fpath)
                for j, snr_range in enumerate(INTERNET_SNRs):
                    snr_ = np.random.uniform(low=snr_range[0], high=snr_range[1])
                    mix_ = mix(speech, noise2, snr_)

                    np.save(os.path.join(SYNTHESIZED_DATA_PATH, 'mixture', f'{speech_fname}_internet_{j}.npy'), mix_)
        except:
            pass 

    # Create valid dataset 
    val_male_spkrs = np.load(os.path.join(META_DATA_DIR, 'val_male_speakers.npy'))
    val_female_spkrs = np.load(os.path.join(META_DATA_DIR, 'val_female_speakers.npy'))
    val_spkrs = np.hstack((val_male_spkrs, val_female_spkrs))

    for i, spkr in enumerate(val_spkrs):
        print(f'## speaker {i}th ...')
        try:
            for fname in os.listdir(os.path.join(SPEECH_PATH, spkr)):
                # speech
                fpath = os.path.join(SPEECH_PATH, spkr, fname)
                speech = read_wave(fpath)
                ## save clean speech 
                np.save(os.path.join(SYNTHESIZED_DATA_PATH, 'valid_clean', fname.split('.')[0] + '.npy'), speech)
                # noise
                ## noise huynv22
                this_type_list = noise_file_dict[noise_types[np.random.randint(len(noise_types))]]
                noise1_fpath = os.path.join(NOISE_BY_HUYNV22, this_type_list[np.random.randint(len(this_type_list))])
                noise1 = read_wave(noise1_fpath)
                ## noise internet
                noise2_fpath = os.path.join(NOISE_FROM_INTERNET, internet_to_use[np.random.randint(len(internet_to_use))])
                noise2 = read_wave(noise2_fpath)
                # mix 
                snr1 = np.random.uniform(low=HUYNV22_SNRs[0][0], high=HUYNV22_SNRs[-1][-1])
                mix1 = mix(speech, noise1, snr1)
                ## save mix1
                np.save(os.path.join(SYNTHESIZED_DATA_PATH, 'valid_mixture', fname.split('.')[0] + '_huynv22.npy'), mix1)
                snr2 = np.random.uniform(low=INTERNET_SNRs[0][0], high=INTERNET_SNRs[-1][-1])
                mix2 = mix(speech, noise2, snr2)
                ## save mix2
                np.save(os.path.join(SYNTHESIZED_DATA_PATH, 'valid_mixture', fname.split('.')[0] + '_internet.npy'), mix2)
        except:
            pass 

if __name__ == '__main__':
    mix_noise_and_preprocess()