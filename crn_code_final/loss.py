import os
import numpy as np

import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from torch.nn.utils.rnn import pad_sequence

def mse_loss():
    def loss_function(est, label, nframes):

        EPSILON = 1e-7
        with torch.no_grad():
            mask_for_loss_list = []
            for frame_num in nframes:
                mask_for_loss_list.append(torch.ones(frame_num, label.size()[2], dtype=torch.float32))
            # input: list of tensor
            # output: B T F
            mask_for_loss = pad_sequence(mask_for_loss_list, batch_first=True).cuda()

        masked_est = est * mask_for_loss
        masked_label = label * mask_for_loss
        loss = ((masked_est - masked_label) ** 2).sum() / mask_for_loss.sum() + EPSILON
        return loss
    return loss_function

